
package caballerolistasenlazadas;

public class CaballeroListasEnlazadas {

   public static void main(String[] args) {
        
      Lista listaenlazada = new Lista();
        listaenlazada.agregarAlInicio("Christopher segovia");
        listaenlazada.agregarAlInicio("Cinthia trejo");
        listaenlazada.agregarAlInicio("Darlin ake");
        listaenlazada.agregarAlInicio("Eduardo duarte");
        listaenlazada.agregarAlInicio("Frida bacab");
        listaenlazada.agregarAlInicio("Raul caballero");
        listaenlazada.agregarAlInicio("Monica tamay");
        listaenlazada.agregarAlInicio("Cecilia pacheco");
        listaenlazada.agregarAlInicio("Brian sierra");
        listaenlazada.agregarAlInicio("Edwin hernandez");
        
        listaenlazada.mostrarListaEnlazada();
     
    }
    }
